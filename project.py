import requests
import pandas as pd
import shutil
import os


# Define latitude, longitude, and number of requested data at each dimension
# The below code does the following job:
# 1. take the given lat,lon as the center
# 2. use given step size (dx) and number of lons and lats (nx,ny respectively)
# 3. prepare two arrays for lons and lats


mainpath = 'data/'
dx = 0.01
lat_center = 41.000
lon_center = 28.000

nx = 3
ny = 3

lats = []
lons = []

for i in range(1, nx):
        lats.append(lat_center+((i-(int(nx*0.5)))*dx))

for i in range(1, ny):
        lons.append(lon_center+((i-(int(ny*0.5)))*dx))

# API call initializations for downloading PV data from renewables.ninja

token = '6b53970d171f4b35f58f95591f116692cfcd9bbe'
api_base = 'https://www.renewables.ninja/api/v1/'

s = requests.session()
# Send token header with each request
s.headers = {'Authorization': 'Token ' + token}
url = api_base + 'data/pv'
args = {
    'lat': 34.125,
    'lon': 39.814,
    'date_from': '2014-01-01',
    'date_to': '2014-12-31',
    'dataset': 'merra2',
    'capacity': 1.0,
    'system_loss': 10,
    'tracking': 0,
    'tilt': 35,
    'azim': 180,
    'format': 'csv'
}

# create directory for the csv files if does not exist.
dname = str(mainpath)+str(lat_center)+"_"+str(lon_center)+"_"+str(nx)+"_"+str(ny)
if not os.path.exists(dname):
    os.makedirs(dname)


for i in lats:
    for k in lons:
        fname = "PV_"+str(i)+"_"+str(k)+".csv"
        path = str(dname)+"/"+str(fname)
        args['lat'] = i
        args['lon'] = k
        with open(path, 'wb') as f:
            r = s.get(url, params=args)
            r.raw.decode_content = True
            for block in r.iter_content(1024):
                f.write(block)
